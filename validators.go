package tablemaker

func validate_body(rows [][]string) error {
	if len(rows) == 0 {
		return nil
	}
	row_0_len := len(rows[0])
	for i, row := range rows {
		if len(row) != row_0_len {
			return &ErrInconsistentRows{
				FirstRowLen: row_0_len,
				BadRowNum:   i,
				BadRowLen:   len(row),
			}
		}
	}
	return nil
}

func validate_header(head []string, body [][]string) error {
	if len(body) == 0 || len(head) == 0 {
		return nil
	}
	if len(head) != len(body[0]) {
		return &ErrHeadInconsistentToBody{
			HeadRowLen: len(head),
			BodyRowLen: len(body[0]),
		}
	}
	return nil
}
