package tablemaker

import "fmt"

type ErrInconsistentRows struct {
	FirstRowLen int
	BadRowNum   int
	BadRowLen   int
}

type ErrHeadInconsistentToBody struct {
	HeadRowLen int
	BodyRowLen int
}

func (e *ErrInconsistentRows) Error() string {
	return fmt.Sprintf(
		"Inconsistent rows (%d != %d) (row num: %d)",
		e.FirstRowLen,
		e.BadRowLen,
		e.BadRowNum,
	)
}

func (e *ErrHeadInconsistentToBody) Error() string {
	return fmt.Sprintf(
		"Header is inconsistent to body(%d != %d)",
		e.HeadRowLen,
		e.BodyRowLen,
	)
}
