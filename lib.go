package tablemaker

type Maker interface {
	SetHeader(columns ...string)
	AddRow(columns ...any)
	SetRowSeparator(sep rune)
	SetHeaderBodySeparator(sep rune)
	SetColumnSeparator(sep string)
	Render() (string, error)
}

func New() Maker {
	return &maker{
		column_separator: " ",
		header:           []string{},
		body:             [][]string{},
	}
}
