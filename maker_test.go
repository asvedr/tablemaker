package tablemaker_test

import (
	"strings"
	"testing"

	"gitlab.com/asvedr/tablemaker"
)

func TestTable(t *testing.T) {
	maker := tablemaker.New()
	maker.AddRow("hi", 2, 3)
	maker.AddRow("make", 4, 5)
	maker.SetColumnSeparator(" | ")
	text, err := maker.Render()
	if err != nil {
		t.Fatal(err)
	}
	expected := strings.Join(
		[]string{
			"hi   | 2 | 3",
			"make | 4 | 5",
		},
		"\n",
	)
	if strings.TrimSpace(text) != strings.TrimSpace(expected) {
		t.Fatalf("\n%s\n\nNOT EQUAL TO\n\n%s", text, expected)
	}
}

func TestHeader(t *testing.T) {
	maker := tablemaker.New()
	maker.AddRow(1, 2, 3)
	maker.AddRow(4, 5, 6)
	maker.SetColumnSeparator(" ")
	maker.SetHeader("a", "b", "c")
	maker.SetHeaderBodySeparator('-')
	text, err := maker.Render()
	if err != nil {
		t.Fatal(err)
	}
	expected := strings.Join(
		[]string{
			"a b c",
			"-----",
			"1 2 3",
			"4 5 6",
		},
		"\n",
	)
	if strings.TrimSpace(text) != strings.TrimSpace(expected) {
		t.Fatalf("\n%s\n\nNOT EQUAL TO\n\n%s", text, expected)
	}
}

func TestRowSep(t *testing.T) {
	maker := tablemaker.New()
	maker.AddRow(1, 2, 3)
	maker.AddRow(4, 5, 6)
	maker.SetColumnSeparator(" | ")
	maker.SetHeader("a", "b", "c")
	maker.SetRowSeparator('-')
	text, err := maker.Render()
	if err != nil {
		t.Fatal(err)
	}
	expected := strings.Join(
		[]string{
			"a | b | c",
			"---------",
			"1 | 2 | 3",
			"---------",
			"4 | 5 | 6",
		},
		"\n",
	)
	if strings.TrimSpace(text) != strings.TrimSpace(expected) {
		t.Fatalf("\n%s\n\nNOT EQUAL TO\n\n%s", text, expected)
	}
}
