package tablemaker

import (
	"fmt"
	"strings"
)

type maker struct {
	row_separator         string
	header_body_separator string
	column_separator      string
	header                []string
	body                  [][]string
}

func (self *maker) SetHeader(columns ...string) {
	self.header = columns
}

func (self *maker) AddRow(columns ...any) {
	strcol := []string{}
	for _, column := range columns {
		strcol = append(strcol, fmt.Sprint(column))
	}
	self.body = append(self.body, strcol)
}

func (self *maker) SetRowSeparator(sep rune) {
	self.row_separator = string(sep)
}

func (self *maker) SetHeaderBodySeparator(sep rune) {
	self.header_body_separator = string(sep)
}

func (self *maker) SetColumnSeparator(sep string) {
	self.column_separator = string(sep)
}

func (self *maker) Render() (string, error) {
	if len(self.body)+len(self.header) == 0 {
		return "", nil
	}
	err := validate_header(self.header, self.body)
	if err != nil {
		return "", err
	}
	err = validate_body(self.body)
	if err != nil {
		return "", err
	}
	lens := self.calculate_lens()
	result := ""
	row_sep := self.row_separator
	hb_sep := self.header_body_separator
	sep_len := self.calculate_sep_len(lens)
	if hb_sep == "" && row_sep != "" {
		hb_sep = row_sep
	}
	if len(self.header) != 0 {
		result = self.render_row(lens, self.header)
		if hb_sep != "" {
			result += make_separator(hb_sep, sep_len)
		}
	}
	if len(self.body) == 0 {
		return result, nil
	}
	result += self.render_row(lens, self.body[0])
	for _, row := range self.body[1:] {
		if row_sep != "" {
			result += make_separator(row_sep, sep_len)
		}
		result += self.render_row(lens, row)
	}
	return result, nil
}

func (self *maker) calculate_lens() []int {
	var result []int
	if len(self.header) != 0 {
		result = get_lens_of_row(self.header)
	} else {
		result = get_lens_of_row(self.body[0])
	}
	for _, row := range self.body {
		merge_lens(result, get_lens_of_row(row))
	}
	return result
}

func (self *maker) calculate_sep_len(lens []int) int {
	sep_len := len(self.column_separator)
	res := sep_len * (len(lens) - 1)
	for _, l := range lens {
		res += l
	}
	return res
}

func (self *maker) render_row(lens []int, row []string) string {
	result := ""
	for i, data := range row {
		if i > 0 {
			result += self.column_separator
		}
		ind := strings.Repeat(" ", lens[i]-len(data))
		result += data + ind
	}
	result += "\n"
	return result
}

func make_separator(hb_sep string, sep_len int) string {
	return strings.Repeat(hb_sep, sep_len) + "\n"
}

func get_lens_of_row(row []string) []int {
	var result []int
	for _, token := range row {
		result = append(result, len(token))
	}
	return result
}

func merge_lens(result []int, row []int) {
	for i, total := range result {
		if row[i] > total {
			result[i] = row[i]
		}
	}
}
